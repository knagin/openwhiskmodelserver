#!/usr/bin/env python
import os
import sys

os.environ["CUDA_VISIBLE_DEVICES"]="0"

import json
import tensorflow as tf
import numpy as np

import warnings
import math

import glob

import time

import json_sequences_iterator as jsi

class ModelWrapper():
    def __init__(self, data_path, model_file, custom_properties):
        ####
        # Setup

        # Location of the model data:
        self.data_path = data_path

        # Trained weights file:
        self.model_file = model_file #data_path + 'weights-360m-cut-large2-censoring_loss.h5'

        # Size of the hidden state
        self.hidden_size = custom_properties["hidden_size"]

        self.__initializeInMemoryLUTs__()

        self.__setupEnvironment__()

        self.__defineTensors__()

        self.__modelMetrics__()

        self.__modelInstance__()

        self.__initializeSession__()

        loaded = False
        self.__loadWeights__()

        self.datatype = "payload"
        self.pipe = jsi.JsonUtils.operationPipe([ jsi.getFeatureMapper(), jsi.getFormatter(self.datatype) ])

    def __initializeInMemoryLUTs__(self):
        ####
        # Initialization of in-memory structures

        self.vocabularies = {
            "geo_agg_code":      set(),
            "entity_class":      set(),
            "entity_id":         set(),
            "problem_class":     set(),
            "entity_technology": set(),
            "origin_subsystem":  set()
        }

        self.vocabularies_to_ids = {
            "geo_agg_code":      {},
            "entity_class":      {},
            "entity_id":         {},
            "problem_class":     {},
            "entity_technology": {},
            "origin_subsystem":  {}
        }

        ####
        # Load the vocabularies from a text file

        for key in self.vocabularies.keys():
            with open(self.data_path + "vocabulary-"+key+".txt","r") as fileHandler:
                for line in fileHandler.readlines():
                    tokens = line.rstrip("\n\r").split('\t')
                    if (len(tokens) >= 2):
                        index = int(tokens[-1])
                        value = '\t'.join(tokens[0:-1])
                        self.vocabularies[key].update([value])
                        self.vocabularies_to_ids[key][value] = index

        for key in self.vocabularies.keys():
            print( "Vocabulary for " + key + " : " + str(len(self.vocabularies[key])) + " entries" )

    def __setupEnvironment__(self):
        ####
        # Environment setup - Clears model graph and session

        tf.reset_default_graph()
        tf.keras.backend.clear_session()
        tf.keras.backend.set_epsilon(1e-8)

    def __defineTensors__(self):
        ####
        # Tensors definition
        read_batch_size = 1

        # The input dimensions number (+2 from dayhour, +2 from weekday, +1 from open flag)
        self.input_dimensions = len(self.vocabularies["problem_class"]) + len(self.vocabularies["entity_technology"]) + 2 + 2 + 1
        print("Input dimensions number: " + str(self.input_dimensions))

        print("Dimensions of the problem_class", len(self.vocabularies["problem_class"]))
        print("Dimensions of the entity_technology", len(self.vocabularies["entity_technology"]))

        #problem_class_list = tf.placeholder(tf.int64, shape=(time_size), name="problem_class_list")
        problem_class_list = tf.placeholder(tf.int64, shape=(None), name="problem_class_list")
        problem_class_tensor = tf.one_hot(
            indices=problem_class_list,
            depth=len(self.vocabularies["problem_class"]), name="one_hot_problem_class")
        problem_class_tensor = tf.reshape(problem_class_tensor ,shape=(read_batch_size, -1, len(self.vocabularies["problem_class"])))


        #entity_technology_list = tf.placeholder(tf.int64, shape=(time_size), name="entity_technology_list")
        entity_technology_list = tf.placeholder(tf.int64, shape=(None), name="entity_technology_list")
        entity_technology_tensor = tf.one_hot(
            indices=entity_technology_list,
            depth=len(self.vocabularies["entity_technology"]), name="one_hot_entity_technology")
        entity_technology_tensor = tf.reshape(entity_technology_tensor ,shape=(read_batch_size, -1, len(self.vocabularies["entity_technology"])))

        #dayhour_tensor = tf.placeholder(tf.float32, shape=(time_size,2), name="dayhour")
        dayhour_tensor = tf.placeholder(tf.float32, shape=(None,None,2), name="dayhour")
        #weekday_tensor = tf.placeholder(tf.float32, shape=(time_size,2), name="weekday")
        weekday_tensor = tf.placeholder(tf.float32, shape=(None,None,2), name="weekday")

        open_tensor = tf.placeholder(tf.float32, shape=(None,None,1), name="open")

        masking_value = tf.constant(np.nan, shape=[self.input_dimensions], dtype=tf.float32)

        # Create the feature set vector
        #feature_set_tensor = tf.concat([problem_class_tensor, entity_technology_tensor],1)
        feature_set_tensor_concat = tf.concat([problem_class_tensor, entity_technology_tensor, dayhour_tensor, weekday_tensor, open_tensor],2)

        feature_set_tensor_masked = tf.map_fn(lambda batch_entry:
            tf.map_fn(lambda record_entry:
                tf.cond(tf.equal(tf.reduce_sum(record_entry), tf.constant(0, dtype=tf.float32)), lambda: masking_value, lambda: record_entry)
            , batch_entry)
        , feature_set_tensor_concat)

        self.feature_set_tensor = tf.reshape(
            feature_set_tensor_masked,
            #shape=(1, time_size, len(vocabularies["problem_class"]) + len(vocabularies["entity_technology"]) + 2 + 2)
            shape=[read_batch_size, -1, self.input_dimensions]
        )

        # defines input tensor as a placeholder
        #input_tensor = tf.placeholder(tf.float32, shape=(1, time_size, input_dimensions), name="input_tensor")
        input_tensor = tf.placeholder(tf.float32, shape=(None, None, self.input_dimensions), name="input_tensor")

    def __modelMetrics__(self):
        ### Model Metrics:

        # Util function
        def indexed_element(records, i):
            #                         batch sequence value
            return tf.slice(records, [0, 0, i], [-1, -1, 1])
            # Equivalent, but a lot slower (6mins vs 40mins)
            #return tf.map_fn(lambda record: record[i], records)

        # mean absolute TTE error
        def mean_tte_error(labels, predictions):
            mean = tf.reduce_mean(tf.abs(tf.subtract(indexed_element(labels,0), indexed_element(predictions,0))), name="mean_tte_error")
            #print("tte mean tensor:", mean)
            return mean

        # mean absolute Censor point error
        def mean_censor_error(labels, predictions):
            mean = tf.reduce_mean(tf.abs(tf.subtract(indexed_element(labels,1), indexed_element(predictions,1))), name="mean_ceansor_error")
            #print("censor mean tensor:", mean)
            return mean

        # Censoring loss function
        def censoring_loss_function(labels, predictions):
            return tf.keras.backend.mean(
                indexed_element(labels,1) * tf.squared_difference(indexed_element(labels,0), indexed_element(predictions,0)) + tf.squared_difference(indexed_element(labels,1), indexed_element(predictions,1)),
            axis=-1)

        # Weighted loss function
        def weighted_loss_function(labels, predictions):
            return tf.keras.backend.mean(
                indexed_element(labels,1) * tf.squared_difference(indexed_element(labels,0), indexed_element(predictions,0)) * tf.reciprocal(
                    indexed_element(labels,0) + 1
                ) + tf.squared_difference(indexed_element(labels,1), indexed_element(predictions,1)),
            axis=-1)

        # Loss function
        def custom_loss_function(labels, predictions):
            return tf.keras.backend.mean(
                (
                    ( ( indexed_element(labels,1) ) * tf.squared_difference( indexed_element(labels,0), indexed_element(predictions,0) ) ) +
                    tf.squared_difference(indexed_element(labels,1), indexed_element(predictions,1))
                ) * tf.reciprocal(
                        indexed_element(labels,0) + 1
                ),
            axis=-1)

        self.metric_functions = {
            "mean_tte_error"   : mean_tte_error,
            "mean_censor_error": mean_censor_error,
            "loss_function"   : censoring_loss_function
        }

    def __modelInstance__(self):
        ####
        # Model Definition

        # Create an instance of the model
        self.model = tf.keras.models.Sequential()
        self.model.add(tf.keras.layers.Masking(mask_value=np.nan, input_shape=(None, self.input_dimensions)))
        # ## TanH input dense/GRU layer activation squashes outliers -> small generalizability effect
        # Using LSTM to tackles vanishing gradients
        rnnLayer = tf.keras.layers.LSTM(self.hidden_size,
                                      #input_shape=(time_size, self.input_dimensions),
                                      activation='tanh',
                                      return_sequences=True,
                                      recurrent_dropout=0,
                                      unroll=False)
        self.model.add(rnnLayer)


        rnnLayer2 = tf.keras.layers.LSTM(self.hidden_size,
                                      #input_shape=(time_size, self.input_dimensions),
                                      activation='tanh',
                                      return_sequences=True,
                                      recurrent_dropout=0,
                                      unroll=False)
        self.model.add(rnnLayer2)

        self.model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(2)))
        self.model.add(tf.keras.layers.Dense(2))

        # Use the Adam optimizer for training
        optimizer = tf.keras.optimizers.Adam(
            lr=0.001,
            beta_1=0.9,
            beta_2=0.999,
            clipvalue=0.5
        )

        self.model.compile(
            metrics=[
                    self.metric_functions["mean_tte_error"],
                    self.metric_functions["mean_censor_error"],
                    'mean_absolute_error'
                ],
            loss=self.metric_functions["loss_function"],
            optimizer=optimizer,
            sample_weight_mode="temporal"
        )
        self.model.summary()

    ####################################
    ### Batch data prepare stage

    def __ingest_data__(self, context_data, sequence_data, sess):
        batch_size = len(sequence_data["entity_id"])
        sequence_size = len(sequence_data["entity_id"][0])

        list_problems = [
                            [ self.vocabularies_to_ids["problem_class"][entry] if entry in self.vocabularies_to_ids["problem_class"] else -1 for entry in record ] for record in sequence_data["problem_class"]
                        ]
        list_technologies = [
                                [ self.vocabularies_to_ids["entity_technology"][entry] if entry in self.vocabularies_to_ids["entity_technology"] else -1 for entry in record ] for record in sequence_data["entity_technology"]
                            ]

        list_problems = np.array(list_problems)
        list_technologies = np.array(list_technologies)

        #list_problems = sequence_data["problem_class"]
        #list_technologies = sequence_data["entity_technology"]

        dayhour = np.reshape(np.tile(context_data["dayhour"], sequence_size),(batch_size, sequence_size,2))
        weekday = np.reshape(np.tile(context_data["weekday"], sequence_size),(batch_size, sequence_size,2))

        #print([record for record in context_data["lastDelta"]])

        # TODO: Change this to properly create the weights without execution errors/warnings:
        problem_flags = (list_problems!=0)*1.
        # Use the last problem of the sequence
        problem_flags[:,:-1] = problem_flags[:,:-1] - problem_flags[:,1:]
        weights = np.reshape( problem_flags, (batch_size, sequence_size))

        # ###

        for record_index in range(batch_size):
            record = sequence_data["entity_technology"][record_index]
            for entry_index in range(sequence_size):
                entry = record[entry_index]
                if weights[record_index][entry_index] == 1 and entry == 0:
                    print("Issue found at record {}, entry {}:".format(record_index, entry_index))
                    print("  {} is present in the problem_class vocabulary, but...".format(sequence_data["problem_class"][record_index][entry_index]))
                    print("  {} is not present in the entity_technology vocabulary!".format(entry))
                    print("Shapes:")
                    print(list_problems.shape)
                    print(list_technologies.shape)
                    print(weights.shape)
                    print("Problems sequence:")
                    print(list_problems[record_index])
                    print("Technologies sequence:")
                    print(list_technologies[record_index])
                    print("Weight map:")
                    print(weights[record_index])


        # ###

        list_problems = np.reshape(list_problems, (batch_size, sequence_size, 1))
        list_technologies = np.reshape(list_technologies, (batch_size, sequence_size, 1))
        dayhour = np.reshape(dayhour, (batch_size, sequence_size, 2))
        weekday = np.reshape(weekday, (batch_size, sequence_size, 2))
        openFlag = np.reshape(sequence_data["open"], (batch_size, sequence_size, 1))

        # One Hot vector encoded problem class in sequence:

        feed_dictionary = {
            "problem_class_list:0": list_problems,
            "entity_technology_list:0": list_technologies,
            "dayhour:0": dayhour,
            "weekday:0": weekday,
            "open:0": openFlag
        }

        try:
            feature_set = sess.run([self.feature_set_tensor],
                feed_dict=feed_dictionary
            )

            return sequence_size, feature_set, weights, batch_size
        except:
            print("A error occurred while encoding the feature and label data.")
            print("Expected input dimensions:", self.input_dimensions)
            print("Dimensions of the problem_class", len(self.vocabularies["problem_class"]))
            print("Dimensions of the entity_technology", len(self.vocabularies["entity_technology"]))
            [print("Shapes: ",k,v.shape) for k,v in feed_dictionary.items()]
            #[display(k,v) for k,v in feed_dictionary.items()]
            print("Weights: ", weights)
            raise

    ####################

    def __initializeSession__(self):
        self.session = tf.Session()
        tf.keras.backend.set_session(self.session)

        global_init_op = tf.initializers.variables(self.session.graph.get_collection(tf.GraphKeys.GLOBAL_VARIABLES))
        local_init_op = tf.initializers.variables(self.session.graph.get_collection(tf.GraphKeys.LOCAL_VARIABLES))
        train_init_op = tf.initializers.variables(self.session.graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))

        self.session.run([global_init_op, local_init_op, train_init_op])

    ####################
    def __loadWeights__(self):
        if os.path.isfile(self.model_file):
            self.model.load_weights(self.model_file)
            print("Loaded model weights from hdf5 file!")
            self.loaded = True
        else:
            print("Model weights were not loaded")
            raise Exception("Model weights not provided")

    def processBatch(self, batch):

        windowStart = batch[0]["windowStart"]
        windowEnd   = batch[0]["windowEnd"]

        (context_data, sequence_data) = self.pipe(batch)
        (sequence_size, feature_set, weights, batch_size) = self.__ingest_data__(context_data, sequence_data, self.session)

        prediction = self.model.predict_on_batch(feature_set)

        (alpha, beta) = prediction[weights==1][0];

        prob = 0 if beta.item() > 1 else 1 - beta.item()
        prob = 1 if prob > 1 else prob

        return {
            "prediction": {
                "TTE": alpha.item()*3600*6,
                "probability": prob,
                "event": prob>0.5
            },
            "context_data": {
                "totalRecords": context_data["totalRecords"][0],
                "dayhour": [context_data["dayhour"][0][0], context_data["dayhour"][0][1]],
                "weekday": [context_data["weekday"][0][0], context_data["weekday"][0][1]],
                "geo_agg_code": sequence_data["geo_agg_code"][0][0],
                "windowStart": windowStart,
                "windowEnd": windowEnd
            }
        }

def main(params):
    ####################
    # Predictions:

    ####
    # Setup

    # Location of the model data

    print("P A R A M S")
    print(params)
    print("-=-=-=-=-")

    if 'data' in params:
        batch = params['data']
    elif 'messages' in params:
        batch = params['messages'][0]['value']
        print("batch")
        print(batch)
        try:
          batch = json.loads(batch)
          print("batch2")
          print(batch)
        except Exception as er:
          print(er)
          batch = json.dumps(batch)
          print("batch3")
          print(batch)
          batch = json.loads(batch)
          print("batch4")
          print(batch)
    else:
        sys.stdout.write("failed to parse input")
        sys.exit(1)

    data_path='./model_data/'

    # Trained weights file:
    model_file = data_path + 'weights-360m-cut-large2-censoring_loss.h5'

    # Size of the hidden state
    hidden_size = 400

    wrapper = ModelWrapper(data_path, model_file, {"hidden_size": hidden_size})

    #jsonfile_name = '/home/teuser/Documents/developments/tensorflow/alarms/historical_data_gold/Tw360s180lc_alarms-gold.json'

    result = {}

    result = wrapper.processBatch(batch)
    if (result["prediction"]["event"]):

        res = {'value': json.dumps(result), 'messageText': json.dumps(result), "message": "success"}

        if 'OUTPUT-TOPIC' in params:
            res['topic'] = params['OUTPUT-TOPIC']
            res['brokers'] = params['KAFKA-BROKER']

        print("hit!")
        print(json.dumps(result))
    else:
        res = {'Result': '', "value": False, "message": "failure"}
    sys.stdout.write("\rDone!                                           \n")
    return res

if __name__ == "__main__":
    main(sys.argv[1:])
