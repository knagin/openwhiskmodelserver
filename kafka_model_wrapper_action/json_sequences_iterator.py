#!/usr/bin/env python
import sys
import json

import warnings
import math

import glob
import os

import random

####
# Utility classes

class JsonUtils(object):
    def payloadSeqToColumnar(batch, payloadField, padding=False, paddingFields=None):
        """
        Turns a batch of objects with an internal sequence in a payloadField, ex:
        [... 
            {...
                rootField1: ...
                ...
                payloadField: [ ...
                    {...
                        sequenceField1: ...
                    }
                ]
            }
        ]
        into a columnar format of the sequences, ex:
        context_data: {
            rootField1: [ /entry in batch/ , ... ]
            ...
        }
        sequence_data: {
            sequenceField1: [ [ sequence for field ] , ... ]
            ...
        }
        """

        context_collumns = {}
        sequence_collumns = {}
        sequenceMaxSize = 0

        context_data  = {}
        sequence_data = {}

        for entry in batch:

            for key in entry.keys():
                if key != payloadField:
                    context_collumns[key] = type(entry[key])

            seq = entry[payloadField]
            if (len(seq) > sequenceMaxSize):
                sequenceMaxSize = len(seq)

            for key in seq[0].keys():
                sequence_collumns[key] = type(seq[0][key])

        for key in context_collumns:
            context_data[key] = []

        for key in sequence_collumns:
            sequence_data[key] = []

        for entry in batch:

            for key in context_data:
                context_data[key].append(entry[key])

            seq = entry[payloadField]
            batch_sequences = {}

            for key in sequence_collumns:
                batch_sequences[key] = []

            for elem in seq:
                for key in sequence_collumns:
                    if key in elem:
                        batch_sequences[key].append(elem[key])
                    else:
                        batch_sequences[key].append(None)

            if padding:
                for padded in range(len(seq),sequenceMaxSize):
                    for key in sequence_collumns:
                        batch_sequences[key].append(None)

            for key in sequence_collumns:
                sequence_data[key].append(batch_sequences[key])

        return (context_data, sequence_data)

    def payloadSeqToTFRecord(batch, payloadField, padding=False, paddingFields=None):
        """
        Turns a batch of objects with an internal sequence in a payloadField, ex:
        [... 
            {...
                rootField1: ...
                ...
                payloadField: [ ...
                    {...
                        sequenceField1: ...
                    }
                ]
            }
        ]
        into a TFRecord friendly format, ex:
        [
            {
                context: {
                    rootField1: ...
                    ...
                },
                feature_lists: {
                    sequenceField1: [  ... ]
                    ...
                }
            }
        ]
        """
        context_collumns = {}
        sequence_collumns = {}
        sequenceMaxSize = 0

        batch_data = []

        for entry in batch:

            for key in entry.keys():
                if key != payloadField:
                    context_collumns[key] = type(entry[key])

            seq = entry[payloadField]
            if (len(seq) > sequenceMaxSize):
                sequenceMaxSize = len(seq)

            for key in seq[0].keys():
                sequence_collumns[key] = type(seq[0][key])

        for entry in batch:
            entry_data = {
                "context": {},
                "feature_lists": {}
            }
            batch_data.append(entry_data)

            for key in context_collumns:
                if key in entry:
                    entry_data[key] = entry[key]
                else:
                    entry_data[key] = None

            for key in sequence_collumns:
                entry_data["feature_lists"][key] = []

            seq = entry[payloadField]
            for elem in seq:
                for key in sequence_collumns:
                    if key in elem:
                        entry_data["feature_lists"][key].append(elem[key])
                    else:
                        entry_data["feature_lists"][key].append(None)

            if padding:
                for padded in range(len(seq),sequenceMaxSize):
                    for key in sequence_collumns:
                        entry_data["feature_lists"][key].append(None)

        return batch_data

    def tfRecordToColumnar(batch, padding=False):
        """
        Tranforms a TFRecord entry, ex:
        [
            {
                context: {
                    rootField1: ...
                    ...
                },
                feature_lists: {
                    sequenceField1: [  ... ]
                    ...
                }
            }
        ]
        Into a Columnar format:
        context_data: {
            rootField1: [ /entry in batch/ , ... ]
            ...
        }
        sequence_data: {
            sequenceField1: [ [ sequence for field ] , ... ]
            ...
        }
        """
        
        context_collumns = {}
        sequence_collumns = {}
        sequenceMaxSize = 0

        context_data  = {}
        sequence_data = {}

        for entry in batch:
            for key in entry["context"].keys():
                context_collumns[key] = type(entry["context"][key])

            for key in entry["feature_lists"].keys():
                seq = entry["feature_lists"][key]
                sequence_collumns[key] = type(seq[0])

                if (len(seq) > sequenceMaxSize):
                    sequenceMaxSize = len(seq)

        for key in context_collumns:
            context_data[key] = []

        for key in sequence_collumns:
            sequence_data[key] = []

        for entry in batch:

            for key in entry["context"].keys():
                context_data[key] = entry["context"][key]

            sequences = entry["feature_lists"]

            for key in sequence_collumns:
                sequence = []

                if key in sequences:
                    sequence.extend(sequences[key])
                
                if padding:
                    for padded in range(len(sequence),sequenceMaxSize):
                        sequence.append(None)
                
                sequence_data[key].append(sequence)

        return (context_data, sequence_data)

    def operationPipe(fnList):
        """
        Chains a list of operations over a batch of data
        """
        def execute(batch):
            result = batch
            for fn in fnList:
                result = fn(result)
            return result

        return execute

class JsonSequencer(object):
    """Manages the access and parsing to a file with a Json object per line"""
    def __init__(self, filename, batchSize=1):
        self.filename = filename
        self.index = []
        self.fp = None
        self.batchSize = batchSize
        self.iteratorPtr = None

    def __enter__(self):
        self.fp = open(self.filename, 'r').__enter__()
        line = '\n'
        while line != '':
            pointer = self.fp.tell()
            line = self.fp.readline()
            if (line!=''):
                self.index.append(pointer)

        return self

    def __exit__(self, ex_type, ex_value, trace):
        self.index = []
        self.fp.__exit__(ex_type, ex_value, trace)
        self.fp = None

    def __len__(self):
        return len(self.index)

    def __getitem__(self, entry):
        if entry<0 or entry >= len(self.index):
            raise Exception("Entry number out of bounds")
        if self.fp != None:
            self.fp.seek(self.index[entry])
            line = self.fp.readline()
            return json.loads(line)
        else:
            with self as seqr:
                return seqr[entry]

    def __iter__():
        self.iteratorPtr = 0
        self.__enter__()

    def __next__(self):
        self.iteratorPtr += 1
        if self.iteratorPtr > len(self):
            self.__exit__(None, None, None)
            raise StopIteration
        try:
            return self[self.iteratorPtr]
        except:
            self.__exit__(None, None, None)
            raise StopIteration

    def getBatch(self, index, batchSize=None):
        if batchSize == None:
            batchSize = self.batchSize

        batch = []
        for i in range(index, index+batchSize if index+batchSize < len(self.index) else len(self.index)):
                batch.append(self[i])

        return batch;

# converts the data into a given format
def getFormatter(datatype):
    def formatTFRecord(batch):
        return JsonUtils.tfRecordToColumnar(batch, padding=True)

    def formatPayload(batch):
        return JsonUtils.payloadSeqToColumnar(batch, "payload", padding=True)

    datatypes = {
        "TFRecord": formatTFRecord,
        "payload": formatPayload
    }

    return datatypes[datatype]

# Only works for datatype = "payload"
def getFeatureMapper():
    def remap(batch):
        for entry in batch:
            entry["dayhour"] = [
                        math.cos(math.pi * (entry["windowStart"]/3600000 % 24) / 12),
                        math.sin(math.pi * (entry["windowStart"]/3600000 % 24) / 12)
                    ]
            entry["weekday"] = [
                        math.cos(math.pi * (entry["windowStart"]/3600000/24 % 7) / 12),
                        math.sin(math.pi * (entry["windowStart"]/3600000/24 % 7) / 12)
                    ]

            for key in list(entry.keys()):
                if key not in ["totalRecords", "payload", "dayhour", "weekday"]:
                    del entry[key]

            for seqEntry in entry["payload"]:
                for key in list(seqEntry.keys()):
                    if key not in ["open",  "geo_agg_code", "entity_class", "entity_id", "severity", "problem_class", "entity_technology"]:
                        del seqEntry[key]

        return batch

    return remap

#

if __name__ == "__main__":

    #jsonfile_name = 'time_window_alarms-360m-lc-cut-sample.json'
    #datatype = "TFRecord"
    jsonfile_name = '/home/teuser/Documents/developments/tensorflow/alarms/historical_data_gold/Tw360s180lc_alarms-gold.json'
    datatype = "payload"

    # Location of the model data:
    data_path='./model_data/'

    ####
    # Initialization of in-memory structures

    vocabularies = {
        "geo_agg_code":      set(),
        "entity_class":      set(),
        "entity_id":         set(),
        "problem_class":     set(),
        "entity_technology": set(),
        "origin_subsystem":  set()
    }

    vocabularies_to_ids = {
        "geo_agg_code":      {},
        "entity_class":      {},
        "entity_id":         {},
        "problem_class":     {},
        "entity_technology": {},
        "origin_subsystem":  {}
    }

    ####
    # Load the vocabularies from a text file

    for key in vocabularies.keys():
        with open(data_path + "vocabulary-"+key+".txt","r") as fileHandler:
            for line in fileHandler.readlines():
                tokens = line.rstrip("\n\r").split('\t')
                if (len(tokens) >= 2):
                    index = int(tokens[-1])
                    value = '\t'.join(tokens[0:-1])
                    vocabularies[key].update([value])
                    vocabularies_to_ids[key][value] = index

    ####################

    pipe = JsonUtils.operationPipe([ getFeatureMapper(), getFormatter(datatype) ])
    print(pipe)

    batchSize = 1

    with JsonSequencer(jsonfile_name, batchSize=batchSize) as jsonSequencer:
        size = len(jsonSequencer)
        print(size)

        #print("Get a batch of sequences...")
        #print(json.dumps(jsonSequencer.getBatch(1000, batchSize=5), indent=1))

        print("FeatureMap, restructure and add padding...")
        entry = pipe(jsonSequencer.getBatch(1000, batchSize=1))
        print(json.dumps(entry, indent=1))

        for i in range(0, size, batchSize):
            batch = jsonSequencer.getBatch(i)
            pipe(batch)
            sys.stdout.write("\rGetting batch {}        ".format(i/batchSize))

        sys.stdout.write("\rDone!                   \n")
